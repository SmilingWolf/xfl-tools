#!/usr/bin/env python

import sys, os, binascii
from array import array
from cStringIO import StringIO
from insani import *

if len(sys.argv) <> 3:
    print 'Please give an archive filename and a desired output directory on the\ncommand line.'
    sys.exit(0)

def read_entry(infile):
    result = {}
    fileentry = []
    for i in xrange(40):
        fileentry.append(read_unsigned(infile, BYTE_LENGTH))

    filename = array('B')
    for i in xrange(32):
        if fileentry[i] != 0x00:
            filename.append(fileentry[i])
    result["filename"] = filename.tostring()
    result["fileoffset"] = fileentry[32+3] << 24 | fileentry[32+2] << 16 | fileentry[32+1] << 8 | fileentry[32+0]
    result["filesize"] = fileentry[36+3] << 24 | fileentry[36+2] << 16 | fileentry[36+1] << 8 | fileentry[36+0]
    return result

arcfile = open(sys.argv[0x0001], 'rb')
dirname = sys.argv[0x0002]
assert_string(arcfile, '\x4C\x42\x01\x00', ERROR_ABORT)
arcdata_offset = read_unsigned(arcfile)
filenum = read_unsigned(arcfile)
print ('Extracting %d files' % filenum)

indexarray = []
for i in xrange(filenum):
    fileentry = read_entry(arcfile)
    indexarray.append(fileentry)

for elem in xrange(len(indexarray)):
    print 'Extracting %s (%d bytes)' % (indexarray[elem]['filename'], indexarray[elem]['filesize'])
    pathcomponents = indexarray[elem]['filename'].split('/')
    filepath = dirname
    for direct in pathcomponents:
        if not os.path.isdir(filepath):  # Create directory if it's not there
            os.mkdir(filepath)           # Won't do this for the final filename
        filepath = os.path.join(filepath, direct)

    arcfile.seek(indexarray[elem]['fileoffset'] + arcdata_offset + 0x0C, 0)
    data = arcfile.read(indexarray[elem]['filesize'])

    outbuffer = StringIO()
    outbuffer.write(data)
    outfile = open(filepath, "wb")
    outfile.write(outbuffer.getvalue())
    outfile.close

arcfile.close()
