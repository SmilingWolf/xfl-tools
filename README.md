# XFL Tools

Python tools to work with XFL files such as the ones used by Kindred Spirits of the Roof.  
Actually there's just a decompressor. The format was so boring I actually haven't followed through with the reversing of this one.  
I have seen the image files are crippled, so I might eventually reverse that format, but don't count on that.  
Also, a python script that can separate the invalid OGG stream from the valid one might be a nice idea instead of having to use OGGSplit. Oh well.